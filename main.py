import numpy as np
import random

class Game2048:
    n = 0
    game = np.zeros((4,4))

    def __init__(self):
        self.spawn_random()
        self.spawn_random()

    def spawn_random(self):
        available = np.argwhere(self.game == 0)

        selected = random.choice(available)
        self.game[selected[0]][selected[1]] = 2

    def move_left(self):
        self.n += 1
        moved = False

        #iterate over every game line
        for i, line in enumerate(self.game):
            change = -1

            #iterate trough the colunms starting from 0, try to move them to the left
            for j, number in enumerate(line):
                #the most left number dont need to move, neither does the zeroes
                if j != 0 and number != 0:
                    #try to move the nth element on the line to the left
                    for l in range(j, 0, -1):
                        if self.game[i][l-1] == 0:
                            self.game[i][l-1] = self.game[i][l]
                            self.game[i][l] = 0
                            moved = True

                        elif self.game[i][l-1] == self.game[i][l] and change < l-1:
                            self.game[i][l-1] += self.game[i][l]
                            self.game[i][l] = 0
                            #if the element was already added, stop adding
                            change = l-1
                            moved = True

        if moved:
            self.spawn_random()

    def move_right(self):
        self.game = np.rot90(self.game, 2)
        self.move_left()
        self.game = np.rot90(self.game, 2)

    def move_up(self):
        self.game = np.rot90(self.game, 1)
        self.move_left()
        self.game = np.rot90(self.game, 3)

    def move_down(self):
        self.game = np.rot90(self.game, 3)
        self.move_left()
        self.game = np.rot90(self.game, 1)

    def show(self):
        print(self.game)

game = Game2048()

while True:
    game.show()

    key = input()
    if key == 'w':
        game.move_up()
    elif key == 's':
        game.move_down()
    elif key == 'a':
        game.move_left()
    elif key == 'd':
        game.move_right()
